<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    

    <title>Login</title>
    
    <link href="../resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">	
    <link href="../resources/css/style.css" rel="stylesheet">
    <link href="../resources/css/colors/blue.css" rel="stylesheet"> 
    <script src="../resources/bootstrap/js/popper.min.js"></script>
    <script src="../resources/bootstrap/js/bootstrap.min.js"></script>
    
    
    
</head>
<body>
    
     
	  <section id="wrapper">
        <div class="login-register" >        
            <div class="login-box card">
            <div class="card-body">

                    <h3 class="box-title m-b-20">Iniciar Sesi�n</h3>
                    <div class="form-group ">
                        <div class="col-xs-12">                        
                        <input type="text" title="Usuario" class="form-control" />
                    	</div>
                    <div class="form-group">
                        <div class="col-xs-12">
                        <input type="password" title="Password" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="checkbox checkbox-primary pull-left p-t-0">
                            <input id="checkbox-signup" type="checkbox">

                            </div> <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right">
                            <i class="fa fa-lock m-r-5"></i> �Olvidaste tu Contrase�a?</a> </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Ingresar</button>
                        </div>
                    </div>

            </div>
          </div>
        </div>
        
    </section>
   
	
</body>
</html>